function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='user-btn'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var user_button = document.getElementById('user-btn');
            user_button.addEventListener('click', function () {
                document.getElementById("mySidenav").style.width = "250px";
                document.getElementById("main").style.marginLeft = "250px";
            });
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!');
                        window.location = "index.html";
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
            alert("Please login to see the comment below.");
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('bokunohero_list').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value
            });
            post_txt.value = "";
        }
    });

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent Comment</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref('bokunohero_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var btnkey = childSnapshot.key;
                var childData = childSnapshot.val();
                first_count += 1;
                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + "<br><button onclick='gocommand(&apos;"+btnkey+"&apos;)'>postpage</button>" + str_after_content;
                
                //window.alert(childData.email);
            });
            document.getElementById('post_list').innerHTML = total_post.join('');

            var postpage = document.getElementById('btnpostpage');
            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var btnkey = data.key;
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + "<br><button onclick='gocommand(&apos;"+btnkey+"&apos;)'>postpage</button>" + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    notifyMe();
                }
            });
        })
        .catch(e => console.log(e.message));

        document.addEventListener('DOMContentLoaded', function () {
            if (!Notification) {
                alert('Desktop notifications not available in your browser. Try Chromium.'); 
                return;
            } 
            if (Notification.permission !== "granted")
                Notification.requestPermission();
        });
                  
        function notifyMe() {
            if(1){
                if (Notification.permission !== "granted")
                    Notification.requestPermission();
                else {
                    var notification = new Notification('Notification', {
                        //icon: 'ore.jpg',
                        body: "A message have been send to bokunohero comment page in Anime forum",
                    });
                    notification.onclick = function () {
                        window.open("bokunohero.html");      
                    };
                }
            }       
        }
}

window.onload = function () {
    init();
}

function gocommand(btnkey){
    window.location = "postpage2.html"+'?'+btnkey;
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0px";
} 