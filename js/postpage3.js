function init() {
    var query = window.location.search;
    if (query.substring(0, 1) == '?') {
        query = query.substring(1);
    }
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item' id='user-btn'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var user_button = document.getElementById('user-btn');
            user_button.addEventListener('click', function () {
                document.getElementById("mySidenav").style.width = "250px";
                document.getElementById("main").style.marginLeft = "250px";
            });
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!');
                        window.location = "index.html";
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref(query);
            newpostref.push({
                email: user_email,
                data: post_txt.value
            });
            post_txt.value = "";
        }
        history.go(0);
    });

    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div>\n";


    var postsRef = firebase.database().ref('shokugeki_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                if(query==childSnapshot.key){
                    total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                    first_count += 1;
                    //alert("1");
                }
                //window.alert(childData.email);
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            
            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(query==childSnapshot.key){
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                        document.getElementById('post_list').innerHTML = total_post.join('');
                        //alert("2");
                    }
                }
            });
        })
        .catch(e => console.log(e.message));

    
    var postsRef2 = firebase.database().ref(query);
    var total_post2 = [];
    var first_count2 = 0;
    postsRef2.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                first_count2 += 1;
                //alert("2");
                //window.alert(childData.email);
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
        })
        .catch(e => console.log(e.message));

}



window.onload = function () {
    init();
}

var undercommentref = firebase.database().ref(query);
        undercommentref.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshat){
                var childData = childSnapshat.val();
                total_post[total_post.length] = str_before_username2 + childData.email + "</strong>" + childData.data + str_after_content;
                first_count += 1;
            })
            document.getElementById('post_list').innerHTML = total_post.join('');
        })
        .catch(e => console.log(e.message));